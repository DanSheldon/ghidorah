var j = jQuery;
var jiraFrame, trackerFrame, $subTaskTracker, apiBase;

init();

function init() {
	buildFrames();
	initTrackerFrame();


	var ticket;
	var parentIssue = j('#parent_issue_summary');

	if (parentIssue.length > 0) {
		var parentIssueText = parentIssue.text();
		ticket = parentIssueText.split(' ')[0];
	} else {
		ticket = location.pathname.split('/').pop();
	}

	apiBase = location.origin + "/rest/api/2/";

	getStories();
}

function initTrackerFrame() {
	j('body', trackerFrame.document).append('<div style=\'background: #fff; padding: 10px; z-index: 1000; overflow: scroll; box-sizing:border-box; float:left;\' id=\'sub-task-tracker\'></div>');
	$subTaskTracker = j('#sub-task-tracker', frames['tracker-frame'].document);

	applyFrameCSS();
}

function buildFrames() {
	var currentIssueUrl = window.location.href;

	var frameset = document.createElement('frameset');
	trackerFrame = document.createElement('frame');
	jiraFrame = document.createElement('frame');

	frameset.cols = "30%,70%";

	jiraFrame.src = currentIssueUrl;
	jiraFrame.id = "jira-frame";
	jiraFrame.name = "jira-frame";

	trackerFrame.id = "tracker-frame";
	trackerFrame.name = "tracker-frame";

	frameset.appendChild(trackerFrame);
	frameset.appendChild(jiraFrame);

	j('body').replaceWith(frameset);
	j('body').css({'height':'100%', 'overflow':'hidden'});
	j('html').css('height', '100%');

	trackerFrame = frames['tracker-frame'];
	jiraFrame = frames['jira-frame'];
}

function getStories() {
	var jql = 'filter="Filter for Team Early 80s 2015" and type=Story and status=Open and id not in (YWS-563, YWS-562, YWS-564) ORDER BY rank';
	var url = location.origin + "/rest/api/2/search?jql=" + encodeURIComponent(jql);

	j.get(url, processStories)
}

function processStories(data) {
	$subTaskTracker.html("");
	var output = "";
	output += "<h3>Stories</h3>";
	output += "<ul>";
	

	console.log(data);

	for (var index in data.issues) {
		var story = data.issues[index];
		output += "<li data-key='" + story.key + "'><a data-key='" + story.key + "' class='expand-story' href='#'>[+]</a> <a title='" + story.key + "' href='" + location.origin + "/browse/" + story.key + "' class='jira-frame-update'>" + story.fields.summary + '</a>';
		output += processSubTasks(story.fields.subtasks);
		output += "</li>";
	}

	output += "</ul>";

	$subTaskTracker.append(output);

	addControls();
	addListeners();
}

function addControls() {
	$subTaskTracker.append("<a href='#' class='reload'>Reload Tasks</a><br>");
	$subTaskTracker.append("<a href='#' class='remove'>Remove Frame</a>")
}

function getSubtasks(key) {
	var issueURL = location.origin + '/rest/api/2/issue/' + key;
	j.get(issueURL, processSubTasks);
}

function processSubTasks(subtasks) {
	var output = "<ul>";

	for (var index in subtasks) {
		var subtask = subtasks[index];
		var subtaskUrl = location.origin + '/browse/' + subtask.key;

		if (subtaskUrl == location.href) {
			output += '<li><a href=\'' + subtaskUrl + '\' title=\''+ subtask.key +'\' class=\'sub-task-link active jira-frame-update\' style=\'overflow:hidden;white-space:nowrap\'>&nbsp;&nbsp;&nbsp;' + subtask.fields.summary + '</a></li>';
		} else {
			output += '<li><a href=\'' + subtaskUrl + '\' title=\''+ subtask.key +'\' class=\'sub-task-link jira-frame-update\' style=\'overflow:hidden;white-space:nowrap\'>&nbsp;&nbsp;&nbsp;' + subtask.fields.summary + '</a></li>';
		}
	}

	output += "</ul>";

	return output;
}

function addListeners() {
	var noCheck = false;
	j($subTaskTracker.find('.jira-frame-update')).click(function(event) {
		var $this = j(this);
		j('.jira-frame-update', $subTaskTracker).removeClass('active');
		$this.addClass('active');
		frames['jira-frame'].location = j(this).attr('href');
		return false;
	})

	j($subTaskTracker.find('.expand-story')).click(function(event) {
		var $this = j(this);
		var $parent = $this.closest('li');
		if ($parent.hasClass('expanded')) {
			$parent.removeClass('expanded');
			$this.text("[+]");
		} else {
			$parent.addClass('expanded');
			$this.text("[-]");
		}
		return false;
	})

	//$subTaskTracker.append(' <a href=\'#\' class=\'remove\'>remove</a> ')

	j($subTaskTracker.find('.remove')).click(function() {
		noCheck = true;
		window.location = frames['jira-frame'].location.href;
		return false;
	})

	j($subTaskTracker.find('.reload')).click(function() {
		getStories();
		return false;
	})

	window.onbeforeunload = function(event) {
		if (noCheck) {
			console.log("hi");
			return;
		}
		var message = "Navigating away will remove the sidebar.";
		return message;
	}
}

function applyFrameCSS() {
	var frame = frames['tracker-frame'].document;
	var style = document.createElement('style');
	style.type = "text/css";
	style.innerHTML = ""+
		"a {" +
		"	color: #136e90;" +
		"	text-decoration: none;" +
		"}" +
		"a:hover {"+
		"	color: #1b9dd0" +
		"}" +
		"a.active {"+
		"	color: #000" +
		"}" +
		"li {"+
		"	list-style: none;"+
		"	padding: 4px 0;"+
		"}"+
		"ul {"+
		"	padding-left: 5px;"+
		"	white-space: nowrap;" +
		"}"+
		"ul ul {"+
		"	display: none;"+
		"	padding-top: 10px;"+
		"	padding-left: 10px;"+
		"}"+
		"ul li.expanded ul {"+
		"	display: block"+
		"}"+
		"li.expanded {"+
		"	margin-bottom: 3px;"+
		"}"+
		"li.expanded > a {"+
		"	border-bottom: 1px solid #136e90"+
		"}";

	frame.head.appendChild(style);
}

function retrieveSubtasks(data) {
	if (data.fields) {
		if (data.fields.subtasks) {
			return data.fields.subtasks;
		}
	}

	return null;
}