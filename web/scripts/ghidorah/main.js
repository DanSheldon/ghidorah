var j;
var scriptHost = "https://spotx.danielsheldon.net/scripts/ghidorah/"
var jiraFrame, trackerFrame, $ghidorah, apiBase;

init();

function init() {
    if(!runCompatabilityChecks()) {
        return false;
    }

    j = jQuery;

    buildFrames();

    apiBase = location.origin + "/rest/api/2/";
}

function initTrackerFrame() {
    var body = j('body', trackerFrame.contentWindow.document);
    body.append('<div style=\'background: #fff; z-index: 1000; overflow: scroll; width: 100%; box-sizing:border-box; float:left;\' id=\'ghidorah\'></div>');
    $ghidorah = j('#ghidorah', trackerFrame.contentWindow.document);

    applyFrameCSS();
    buildUI();
    getStories();
}

function buildUI() {

    // Select Filter
    var previousSelectValue = window.localStorage.getItem('selectedFilter');

    var heading = document.createElement('h1');
    var headingContent = document.createTextNode("Ghidorah");

    heading.id = "ghidorah-heading";
    heading.appendChild(headingContent);

    $ghidorah.append(heading);

    addSpacer();

    var selectHeading = document.createElement('h2');
    var selectHeadingContent = document.createTextNode('Filter Select');

    selectHeading.appendChild(selectHeadingContent);

    $ghidorah.append(selectHeading);

    var filters = {
        "Early 80s":        "Filter for Team Early 80s 2015",
        "Film":             "Filter for Team Film 2015",
        "G33k":             "Filter for Team G33k 2015",
        "Hardcore":         "Filter for Team Hardcore 2015",
        "Inyourendo":       "Filter for Team Innuendo 2015",
        "Jukebox":          "Filter for Team Jukebox 2015",
        "Late 80s":         "Filter for Team Late 80s 2015",
        "Lee's in Charge":  "Filter for Team Lee's in Charge",
        "Mayhem":           "Filter for Team Mayhem",
        "Ninja":            "Filter for Team Ninja 2015",
        "Ninja 2":          "Filter for Team Ninja 2015 2",
        "Prototype":        "Filter for Team Prototype"
    };

    var select = document.createElement('select');
    select.id = "filter-select";

    for (var index in filters) {
        var filter = filters[index];
        var option = document.createElement('option');
        var optionText = document.createTextNode(index);

        option.value = filter;
        option.label = index;

        if (filter == previousSelectValue) {
            option.selected = true;
        }

        option.appendChild(optionText);
        select.appendChild(option);
    }

    $ghidorah.append(select)

    addSpacer();

    var storyHeading = document.createElement('h2');
    storyHeading.id = "story-heading";

    var storyHeadingContent = document.createTextNode('Stories');

    var storyDetailsLabel = document.createElement('label');
    storyDetailsLabel.className = "option-label";
    storyDetailsLabel.htmlFor = "details-checkbox";
    storyDetailsLabel.id = "details-checkbox-label";
    storyDetailsLabel.textContent = "Details";

    var storyDetailsCheckbox = document.createElement('input');
    storyDetailsCheckbox.type = "checkbox";
    storyDetailsCheckbox.className = "details-checkbox";

    storyDetailsLabel.appendChild(storyDetailsCheckbox);

    storyHeading.appendChild(storyHeadingContent);
    storyHeading.appendChild(storyDetailsLabel);

    $ghidorah.append(storyHeading);

    j(select).change(function() {
        window.localStorage.setItem('selectedFilter', this.value);
        getStories();
    })

    // Stories/Tasks

    var tasks = document.createElement('div');
    tasks.id = "task-wrapper";

    $ghidorah.append(tasks);

    addControls();
    addListeners();
}

function addSpacer() {
    var spacer = document.createElement('div');
    spacer.className = 'spacer';
    $ghidorah.append(spacer);
}

function buildFrames() {
    
    var width = parseInt(window.localStorage.getItem('trackerWidth')) || 30;
    var currentUrl = window.location.href;

    var frameset = document.createElement('frameset');
    trackerFrame = document.createElement('frame');
    jiraFrame = document.createElement('frame');

    frameset.cols = width + "%,*";

    jiraFrame.src = "about:blank";
    jiraFrame.id = "jira-frame";
    jiraFrame.name = "jira-frame";

    trackerFrame.src = "about:blank";
    trackerFrame.id = "tracker-frame";
    trackerFrame.name = "tracker-frame";

    frameset.appendChild(trackerFrame);
    frameset.appendChild(jiraFrame);

    jiraFrame.onload = function() {
        jiraFrame.contentWindow.location = currentUrl;
        jiraFrame.onload = null;
    }

    trackerFrame.onload = function() {
        initTrackerFrame();
        trackerFrame.onload = null;
    }

    j('body').replaceWith(frameset);
    //j('body').css({'height':'100%', 'overflow':'hidden'});
    //j('html').css('height', '100%');

    //trackerFrame = frames['tracker-frame'];
    //jiraFrame = frames['jira-frame'];
}

function getStories() {
    var filter = $ghidorah.find('#filter-select').val();
    var jql = 'filter="' + filter + '" and type=Story and status=Open ORDER BY rank';
    var url = location.origin + "/rest/api/2/search?jql=" + encodeURIComponent(jql);

    j.get(url, processStories);
}

function processStories(data) {
    var div = $ghidorah.find('#task-wrapper');
    var taskElement = $ghidorah.find('#tasks');

    if (taskElement.length == 0) {
        taskElement = document.createElement('div');;
        taskElement.id = 'tasks';
        taskElement = j(taskElement);
        div.append(taskElement);
    }

    var taskList = document.createElement('ul');

    taskElement.empty();
    taskElement.append(taskList);

    for (var index in data.issues) {
        var story = data.issues[index];
        
        var li = document.createElement('li');
        li.dataset.key = story.key;

        var expandStory = document.createElement('a');
        expandStory.dataset.key = story.key;
        expandStory.className = 'story-subtask-count';
        expandStory.textContent = story.fields.subtasks.length;

        if (story.fields.subtasks.length == 0) {
            expandStory.className += " no-stories";
        } else {
            expandStory.className += " expand-story"
            expandStory.href = "#";
        }

        var storyLink = document.createElement('a');
        storyLink.title = story.key;
        storyLink.href = location.origin + "/browse/" + story.key;
        storyLink.className = "jira-frame-update task-link";
        storyLink.textContent = story.fields.summary;

        li.appendChild(expandStory);
        li.appendChild(storyLink);
        li.appendChild(processSubTasks(story.fields.subtasks));

        taskList.appendChild(li);
    }
}

function updateSubtasks(container) {
    var subtasks = j(container.find('.sub-task-link'));
    subtasks.each(function(index, subtask) {
        j.get(apiBase + 'issue/' + subtask.dataset.key, processSubtaskDetails);
    })
}

function processSubtaskDetails(data) {
    var $subtaskLi = j($ghidorah.find('li[data-key="' + data.key + '"]'));
    var $estimateTotals = j($ghidorah.find('li[data-key="' + data.fields.parent.key + '"] .estimate-totals'));
    var details = $subtaskLi.find('.details'), assignee, estimated, remaining, logged;
    var totals = {};
    
    if (details.length == 0) {
        var details = document.createElement('div');
        details.className = 'details';
        $subtaskLi.append(details);

        assignee = document.createElement('span');
        estimated = document.createElement('span');
        remaining = document.createElement('span');
        logged = document.createElement('span');

        assignee.className = 'assignee';
        estimated.className = 'estimated';
        remaining.className = 'remaining';
        //logged.className = 'logged';

        details.appendChild(assignee);
        details.appendChild(estimated);
        details.appendChild(remaining);
        //details.appendChild(logged);
    } else {
        assignee = $subtaskLi.find('.assignee')[0];
        estimated = $subtaskLi.find('.estimated')[0];
        remaining = $subtaskLi.find('.remaining')[0];
        //logged = $subtaskLi.find('.logged')[0];
    }

    var dName = data.fields.assignee.displayName;

    var assigneeTime = $estimateTotals.find('div[data-assignee="' + dName + '"] .time')[0];
    if (!assigneeTime) {
        var assigneeDiv = document.createElement('div');
        assigneeDiv.dataset.assignee = dName;

        var assigneeName = document.createElement('span');
        var assigneeTime = document.createElement('span');

        assigneeName.className = 'name';
        assigneeTime.className = 'time';

        assigneeName.textContent = dName;
        assigneeTime.dataset.time = data.fields.timetracking.originalEstimateSeconds; 
        
        assigneeDiv.appendChild(assigneeName);
        assigneeDiv.appendChild(assigneeTime);
        
        $estimateTotals.append(assigneeDiv);
    } else {
        assigneeTime.dataset.time = parseInt(assigneeTime.dataset.time) + data.fields.timetracking.originalEstimateSeconds;
    }

    assigneeTime.textContent = secondsToHours(assigneeTime.dataset.time);

    assignee.textContent = dName;
    estimated.textContent = "e: " + data.fields.timetracking.originalEstimate;
    remaining.textContent = "r: " + data.fields.timetracking.remainingEstimate;
}

function secondsToHours(seconds) {
    var hours = seconds / 3600;
    hours = hours + "h";
    return hours;
}

function addControls() {
    $ghidorah.append("<a href='#' class='reload'>Reload Tasks</a><br>");
    $ghidorah.append("<a href='#' class='remove'>Remove Frame</a>")
}

function getSubtasks(key) {
    var issueURL = location.origin + '/rest/api/2/issue/' + key;
    j.get(issueURL, processSubTasks);
}

function processSubTasks(subtasks) {
    var subtaskList = document.createElement('ul');

    for (var index in subtasks) {
        var subtask = subtasks[index];
        var subtaskUrl = location.origin + '/browse/' + subtask.key;

        var subtaskLi = document.createElement('li');
        var subtaskLink = document.createElement('a');

        subtaskLi.dataset.key = subtask.key;
        if (subtask.fields.issuetype.name.indexOf('Bug') > -1) {
            subtaskLi.className = "bug";
        }

        subtaskLink.href = subtaskUrl;
        subtaskLink.title = subtask.key;
        subtaskLink.dataset.key = subtask.key;
        subtaskLink.className = "sub-task-link jira-frame-update";
        subtaskLink.textContent = subtask.fields.summary;

        if (subtaskUrl == location.href) {
            subtaskLink.className += " active";
        }

        subtaskLi.appendChild(subtaskLink);
        subtaskList.appendChild(subtaskLi);
    }

    var estimateTotalsLi = document.createElement('li');
    estimateTotalsLi.className = 'estimate-totals';
    estimateTotalsLi.textContent = "Estimate Totals";
    subtaskList.appendChild(estimateTotalsLi);

    return subtaskList;
}

function addListeners() {
    var noCheck = false;
    j($ghidorah.find('#task-wrapper')).on('click', '.jira-frame-update', function(event) {
        var $this = j(this);
        j('.jira-frame-update', $ghidorah).removeClass('active');
        $this.addClass('active');
        frames['jira-frame'].location = j(this).attr('href');
        return false;
    })

    j($ghidorah.find('#task-wrapper')).on('click', '.expand-story', function(event) {
        var $this = j(this);
        var $parent = $this.closest('li');
        if ($parent.hasClass('expanded')) {
            $parent.removeClass('expanded');
        } else {
            $parent.addClass('expanded');
            updateSubtasks($parent);
        }
        return false;
    })

    j($ghidorah.find('.remove')).click(function() {
        noCheck = true;
        window.location = frames['jira-frame'].location.href;
        return false;
    })

    j($ghidorah.find('.reload')).click(function() {
        getStories();
        return false;
    })

    window.onbeforeunload = function(event) {
        if (noCheck) {
            return;
        }
        var message = "Navigating away will remove the sidebar.";
        return message;
    }

    j($ghidorah.find('#details-checkbox-label')).click(function() {
        var $taskWrapper = j($ghidorah.find('#task-wrapper'));
        j(this).toggleClass('active');
        $taskWrapper.toggleClass('show-details');
    })

    j(trackerFrame.contentWindow).resize(function() {
        var percent = j(this).width()/j(window).width();
        percent = Math.round(percent * 100);
        window.localStorage.setItem('trackerWidth', percent);
    })
}

function applyFrameCSS() {
    var cssId = 'myCss';
    if (!document.getElementById(cssId))
    {
        var head  = document.getElementsByTagName('head')[0];
        var link  = document.createElement('link');
        link.id   = cssId;
        link.rel  = 'stylesheet';
        link.type = 'text/css';
        link.href = scriptHost + 'styles.css';
        link.media = 'all';
        trackerFrame.contentWindow.document.head.appendChild(link);
    }
}

function runCompatabilityChecks() {
    
    if (!checkForJira()) {
        alert("Please navigate to Jira before using Ghidorah");
        return false;
    }

    if (!checkForJquery()) {
        alert("Unable to locate JQuery. Please try again later")
        return false;
    }

    return true;
}

function checkForJquery() {
    if(window["jQuery"]) {
        return true;
    } else {
        return false;
    }
}

function checkForJira() {
    if (location.origin.indexOf('jira') > -1) {
        return true;
    } else {
        return false;
    }
}
